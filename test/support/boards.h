#define BSP_BOARD_LED_0 0
#define BSP_BOARD_LED_1 1

/**
 * Function for inverting the state of an LED.
 *
 * @param led_idx LED index (starting from 0), as defined in the board-specific header.
 */
void bsp_board_led_invert(uint32_t led_idx);