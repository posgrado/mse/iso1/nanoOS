#ifndef UART_H
#define UART_H
#include <stdint.h>
#include "app_uart.h"

void uart_error_handle(app_uart_evt_t *p_event);
void uart_init(uint32_t err_code);
void print_msg(uint8_t nro_led, uint32_t t1, uint32_t t2);

#endif // UART_H
