/****************************************************************************
*	Avoid multiple inclusion - begin                                        *
*****************************************************************************/
#ifndef _NANOOS_H_
#define _NANOOS_H_

/**
 * @file nanoOS.h
 * @author David Broin <davidmbroin@gmail.com>
 * @date 2021/07/26
 * @brief Very slim OS implementation.
 * @copyright All rights reserved.
 * @copyright @ref [MIT LICENSE](LICENSE.txt)
 */

/****************************************************************************
*	Inclusions of public function dependencies                              *
*****************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include "nrf52.h"

/****************************************************************************
*	C++ - begin                                                             *
*****************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

/****************************************************************************
*	Definition macros of public constants                                   *
*****************************************************************************/

#define STACK_SIZE 1024
#define STACK_SIZE_WORDS (STACK_SIZE / 4)
#define STACK_FRAME_SIZE 8
#define FULL_STACKING_SIZE 17 //16 core registers + valor previo de LR
#define MAX_TASK_COUNT 8      //cantidad maxima de tareas para este OS

#define MAX_PRIORITY 0                                   //maxima prioridad que puede tener una tarea
#define MIN_PRIORITY 3                                   //minima prioridad que puede tener una tarea
#define PRIORITY_COUNT (MIN_PRIORITY - MAX_PRIORITY) + 1 //cantidad de prioridades asignables
#define QUEUE_HEAP_SIZE 64                               //cantidad de bytes reservados por cada cola definida
#define CANT_IRQ 39

/****************************************************************************
 * 	Posiciones dentro del stack frame de los registros que conforman 
 * el stack frame
 ****************************************************************************/

#define XPSR 1
#define PC_REG 2
#define LR 3
#define R12 4
#define R3 5
#define R2 6
#define R1 7
#define R0 8
#define LR_PREV_VALUE	9

/*****************************************************************************
 * 			Valores necesarios para registros del stack frame inicial
 ****************************************************************************/

#define INIT_XPSR 1 << 24	   //xPSR.T = 1
#define EXEC_RETURN 0xFFFFFFF9 //retornar a modo thread con MSP, FPU no utilizada


/********************[definicion codigos de error de OS]*********************/
#define ERR_OS_CANT_TAREAS		-1
#define ERR_OS_SCHEDULING		-2

/****************************************************************************
*	Public function-like macros                                             *
*****************************************************************************/

/****************************************************************************
*	Definitions of public data types                                        *
*****************************************************************************/
typedef enum
{
    TASK_READY,
    TASK_RUNNING,
    TASK_BLOCKED
} task_state;

/*****************************************************************************
 * Definicion de los estados posibles de nuestro OS
 ****************************************************************************/
typedef enum
{
    NANOOS_FROM_RESET, //inicio luego de un reset
    NANOOS_NORMAL_RUN, //estado del sistema corriendo una tarea
    NANOOS_SCHEDULING, //el OS esta efectuando un scheduling
    NANOOS_IRQ_RUN     //El OS esta corriendo un Handler
} nanoOS_state;

/********************************************************************************
 * Definicion de la estructura para cada tarea
 *******************************************************************************/
typedef struct
{
    uint32_t stack[STACK_SIZE / 4];
    uint32_t stack_pointer;
    void *entry_point;
    task_state state;
    uint8_t index;	
    uint32_t blocked_ticks;	
    uint8_t priority;
} Task;

/********************************************************************************
 * Definicion de la estructura de control para el sistema operativo
 *******************************************************************************/
typedef struct
{
    Task *tasks_list[MAX_TASK_COUNT];              //array de punteros a tareas
    uint8_t tasks_counter;                         //cantidad de tareas definidas por el usuario para cada prioridad
    int32_t error;                                 //variable que contiene el ultimo error generado
    nanoOS_state system_state;                     //Informacion sobre el estado del OS
    Task *current_task;                            //definicion de puntero para tarea actual
    Task *next_task;                               //definicion de puntero para tarea siguiente
    bool is_context_switch_needed;                 //Esta bandera indica si el scheduler determino un cambio de contexto
    bool scheduling_from_IRQ;                      //esta bandera se utiliza para la atencion a interrupciones
    uint8_t task_priority_counter[PRIORITY_COUNT]; //cada posicion contiene cuantas tareas tienen la misma prioridad
    uint16_t critical_counter;                     //Contador de secciones criticas solicitadas
} NanoOS_control;

/********************************************************************************
 * Definicion de la estructura para los semaforos
 *******************************************************************************/
typedef struct
{
    Task *associated_task;
    bool is_taken;
} Semaphore;

/********************************************************************************
 * Definicion de la estructura para las colas
 *******************************************************************************/
typedef struct
{
    uint8_t data[QUEUE_HEAP_SIZE];
    Task *associated_task;
    uint16_t head_idx;
    uint16_t tail_idx;
    uint16_t element_size;
} Queue;

/****************************************************************************
*	Prototypes (declarations) of public functions                           *
*****************************************************************************/

/***************************************************************************
*  @brief Inicializa las tareas que correran en el OS.
*
*  @details
*   Inicializa una tarea para que pueda correr en el OS implementado. Es
*   necesario llamar a esta funcion para cada tarea antes que inicie el OS.
*
*  @param *entry_point_ptr Puntero a la tarea que se desea inicializar.
*  @param *task_ptr Puntero a la estructura de control que sera utilizada para
*  la tarea que se esta inicializando.
*****************************************************************************/
void nanoOS_task_init(void *entry_point_address, Task *task_ptr, uint8_t priority);

/***************************************************************************
 *  @brief Inicializa el OS.
 *
 *  @details Inicializa el OS seteando la prioridad de PendSV como la mas baja
 *   posible. Es necesario llamar esta funcion antes de que inicie el sistema.
 *   Es recomendable llamarla luego de inicializar las tareas
 *
 *  @param      None.
 *  @return     None.
*****************************************************************************/
void nanoOS_init(void);

/*************************************************************************************************
 *  @brief SysTick Handler.
 *
 *  @details
 *   El handler del Systick no debe estar a la vista del usuario. Dentro se
 *   setea como pendiente la excepcion PendSV.
 *
 *  @param      None.
 *  @return     None.
***************************************************************************************************/

void SysTick_Handler(void);
/*************************************************************************************************
	 *  @brief delay no preciso en base a ticks del sistema
     *
     *  @details
     *   Para utilizar un delay en el OS se vale del tick de sistema para contabilizar cuantos
     *   ticks debe una tarea estar bloqueada.
     *
	 *  @param		ticks	Cantidad de ticks de sistema que esta tarea debe estar bloqueada
	 *  @return     None.
***************************************************************************************************/
void nanoOS_delay(uint32_t ticks);

/*************************************************************************************************
 *  @brief Fuerza una ejecucion del scheduler.
 *
 *  @details
 *   En los casos que un delay de una tarea comience a ejecutarse instantes
 *   luego de que ocurriese un scheduling, se despericia mucho tiempo hasta el
 *   proximo tick de sistema, por lo que se fuerza un scheduling y un cambio de
 *   contexto si es necesario.
 *
 *  @param      None
 *  @return     None.
***************************************************************************************************/
void nanoOS_CPU_yield(void);

/*************************************************************************************************
 *  @brief Funcion para determinar el proximo contexto.
 *
 *  @details
 *   Esta funcion en este momento hace las veces de scheduler y tambien obtiene
 *   el siguiente contexto a ser cargado. El cambio de contexto se ejecuta en el
 *   handler de PendSV, dentro del cual se llama a esta funcion
 *
 *  @param      sp_current   Este valor es una copia del contenido de MSP al
 *              momento en que la funcion es invocada.
 *  @return     El valor a cargar en MSP para apuntar al contexto de la tarea
 *  siguiente.
***************************************************************************************************/
uint32_t getContextoSiguiente(uint32_t sp_current);

/*************************************************************************************************
 *  @brief Hook de retorno de tareas
 *
 *  @details
 *   Esta funcion no deberia accederse bajo ningun concepto, porque ninguna
 *   tarea del OS debe retornar. Si lo hace, es un comportamiento anormal y debe
 *   ser tratado.
 *
 *  @param none
 *
 *  @return none.
***************************************************************************************************/
void return_hook(void);

/*************************************************************************************************
 *  @brief Hook de tick de sistema
 *
 *  @details
 *   Se ejecuta cada vez que se produce un tick de sistema. Es llamada desde el
 *   handler de SysTick.
 *
 *  @param none
 *
 *  @return none.
 *
 *  @warning    Esta funcion debe ser lo mas corta posible porque se ejecuta
 *              dentro del handler mencionado, por lo que tiene prioridad sobre
 *              el cambio de contexto y otras IRQ.
 *
 *  @warning    Esta funcion no debe bajo ninguna circunstancia utilizar APIs
 *              del OS dado que podria dar lugar a un nuevo scheduling.
***************************************************************************************************/
void tick_hook(void);

/*************************************************************************************************
 *  @brief Hook de error de sistema
 *
 *  @details
 *   Esta funcion es llamada en caso de error del sistema, y puede ser utilizada
 *   a fin de hacer debug. El puntero de la funcion que llama a errorHook es
 *   pasado como parametro para tener informacion de quien la esta llamando, y
 *   dentro de ella puede verse el codigo de error en la estructura de control
 *   de sistema. Si ha de implementarse por el usuario para manejo de errores,
 *   es importante tener en cuenta que la estructura de control solo esta
 *   disponible dentro de este archivo.
 *
 *  @param caller       Puntero a la funcion donde fue llamado errorHook.
 *                      Implementado solo a fines de trazabilidad de errores
 *
 *  @return none.
***************************************************************************************************/
void error_hook(const void *caller);

/*************************************************************************************************
 *  @brief Tarea Idle (segundo plano)
 *
 *  @details
 *   Esta tarea se ejecuta solamente cuando todas las demas tareas estan en
 *   estado bloqueado. Puede ser redefinida por el usuario.
 *
 *  @param none
 *
 *  @return none.
 *
 *  @warning        No debe utilizarse ninguna funcion API del OS dentro de esta
 *                  funcion. No debe ser causa de un re-scheduling.
***************************************************************************************************/
void idle_task(void);

/*************************************************************************************************
 *  @brief Inicializacion de un semaforo binario
 *
 *  @details
 *   Antes de utilizar cualquier semaforo binario en el sistema, debe
 *   inicializarse el mismo. Todos los semaforos se inicializan tomados
 *
 *  @param      sem     Semaforo a inicializar
 *  @return     None.
***************************************************************************************************/
void nanoOS_semaphore_init(Semaphore* sem);

/*************************************************************************************************
     *  @brief Tomar un semaforo
     *
     *  @details
     *   Esta funcion es utilizada para tomar un semaforo cualquiera.
     *
     *  @param      sem     Semaforo a tomar
     *  @return     None.
***************************************************************************************************/
void nanoOS_semaphore_take(Semaphore* sem);

/********************************************************************************
 *  @brief Liberar un semaforo
 *
 *  @details
 *   Esta funcion es utilizada para liberar un semaforo cualquiera.
 *
 *  @param      sem     Semaforo a liberar
 *  @return     None.
 *******************************************************************************/
void nanoOS_semaphore_give(Semaphore* sem);

/********************************************************************************
 * Install interrupt. Debemos pasarle el tipo de interrupcion y la funcion del
 * usuario que desea instalar para atender esa interrupcion. La funcion devuelve
 * TRUE si fue exitosa o FALSE en caso contrario
 *******************************************************************************/
bool nanoOS_add_IRQ(IRQn_Type irq, void* usr_isr);

/********************************************************************************
 * Remove interrupt. Debemos pasarle el tipo de interrupcion que queremos quitar
 * del vector que definimos con los punteros a funciones del usuario.
 * La funcion devuelve TRUE si fue exitosa o FALSE en caso contrario
 *******************************************************************************/
bool nanoOS_remove_IRQ(IRQn_Type irq);

/*************************************************************************************************
 *  @brief Inicializacion de una cola
 *
 *  @details
 *   Antes de utilizar cualquier cola en el sistema, debe inicializarse la
 *   misma. Todas las colas se inicializan vacias y sin una tarea asociada. Aqui
 *   se determina cuantos elementos (espacios) tendra disponible la cola dado el
 *   tamaño de la cola en bytes definida por la constante QUEUE_HEAP_SIZE y el
 *   tamaño en bytes de cada elemento que se desea almacenar. Es una forma facil
 *   de determinar los limites de los indices head y tail en el momento de la
 *   operacion. Se puede volver a inicializar una cola para resetearla y cambiar
 *   el tipo de datos que contiene
 *
 *  @param      cola        Cola a inicializar
 *  @param      datasize    Tamaño de los elementos que seran almacenados en la
 *                          cola. Debe ser pasado mediante la funcion sizeof()
 *  @return     None.
 *
 *  @warning    Esta inicializacion fija el tamaño de cada elemento, por lo que
 *              no vuelve a consultarse en otras funciones, pasar datos con
 *              otros tamaños en funciones de escritura y lectura puede dar
 *              lugar a corrupcion de datos.
***************************************************************************************************/
void nanoOS_queue_init(Queue *queue, uint16_t datasize);

/*************************************************************************************************
 *  @brief Escritura en una cola
 *
 *  @details
 *
 *
 *  @param      cola        Cola donde escribir el dato
 *  @param      dato        Puntero a void del dato a escribir
 *  @return     None.
***************************************************************************************************/
void nanoOS_queue_write(Queue *queue, void *data);

void nanoOS_queue_read(Queue *queue, void *data);

/*************************************************************************************************
*  @brief Marca el inicio de una seccion como seccion critica.
*
*  @details
*   Las secciones criticas son aquellas que deben ejecutar operaciones atomicas,
*   es decir que no pueden ser interrumpidas. Con llamar a esta funcion, se
*   otorga soporte en el OS para marcar un bloque de codigo como atomico
*
*  @param      None
*  @return     None
*  @see        os_exit_critical
***************************************************************************************************/
void nanoOS_critical_enter();

/*************************************************************************************************
*  @brief Marca el final de una seccion como seccion critica.
*
*  @details
*   Las secciones criticas son aquellas que deben ejecutar operaciones atomicas,
*   es decir que no pueden ser interrumpidas. Con llamar a esta funcion, se
*   otorga soporte en el OS para marcar un bloque de codigo como atomico
*
*  @param       None
*  @return     None
*  @see         os_enter_critical
***************************************************************************************************/
void nanoOS_critical_exit();

/****************************************************************************
*	Prototypes (declarations) of public interrupt functions                 *
*****************************************************************************/

/****************************************************************************
*	C++ - end                                                               *
*****************************************************************************/

#ifdef __cplusplus
}
#endif

/****************************************************************************
*	Avoid multiple inclusion - end                                          *
*****************************************************************************/

#endif /* _NANOOS_H_ */

