#include "nanoOS.h"
#include <stddef.h>
#include <string.h>

static NanoOS_control nanoOS_control;
static Task idle;
static void *usr_isr_array[CANT_IRQ]; //vector de punteros a funciones para nuestras interrupciones

/*************************************************************************************************
 *  @brief Funcion que efectua las decisiones de scheduling.
 *
 *  @details
 *   Segun el critero al momento de desarrollo, determina que tarea debe
 *   ejecutarse luego, y por lo tanto provee los punteros correspondientes para
 *   el cambio de contexto. Esta implementacion de scheduler es muy sencilla,
 *   del tipo Round-Robin
 *
 *  @param      None.
 *  @return     None.
***************************************************************************************************/
static void scheduler(void);

/*************************************************************************************************
 *  @brief Inicializacion de la tarea idle.
 *
 *  @details
 *   Esta funcion es una version reducida de os_initTarea para la tarea idle.
 *   Como esta tarea debe estar siempre presente y el usuario no la inicializa,
 *   los argumentos desaparecen y se toman estructura y entryPoint fijos.
 *   Tampoco se contabiliza entre las tareas disponibles (no se actualiza el
 *   contador de cantidad de tareas). El id de esta tarea se establece como 255
 *   (0xFF) para indicar que es una tarea especial.
 *
 *  @param      None.
 *  @return     None
 *  @see os_InitTarea
***************************************************************************************************/
static void init_idle_task(void);

/*************************************************************************************************
 *  @brief Setea la bandera correspondiente para lanzar PendSV.
 *
 *  @details
 *   Esta funcion simplemente es a efectos de simplificar la lectura del
 *   programa. Setea la bandera comrrespondiente para que se ejucute PendSV
 *
 *  @param      None
 *  @return     None
***************************************************************************************************/
static void setPendSV(void);

/*************************************************************************************************
 *  @brief Ordena tareas de mayor a menor prioridad.
 *
 *  @details
 *   Ordena los punteros a las estructuras del tipo tarea que estan almacenados
 *   en la variable de control de OS en el array listadoTareas por prioridad, de
 *   mayor a menor. Para esto utiliza un algoritmo de quicksort. Esto da la
 *   posibilidad de cambiar la prioridad de cualquier tarea en tiempo de
 *   ejecucion.
 *
 *  @param      None
 *  @return     None.
***************************************************************************************************/
static void priority_sort(void);

/*************************************************************************************************
 *  @brief Ordena tareas de mayor a menor prioridad.
 *
 *  @details
 *   Funcion de soporte para ordenarPrioridades. No debe llamarse fuera de
 *   mencionada funcion.
 *
 *  @param  arr     Puntero a la lista de punteros de estructuras de tareas a
 *  ordenar
 *  @param  l       Inicio del vector a ordenar (puede ser un subvector)
 *  @param  h       Fin del vector a ordenar (puede ser un subvector)
 *  @return         Retorna la posicion del pivot necesario para el algoritmo
***************************************************************************************************/
static int32_t partition(Task **arr, int32_t l, int32_t h);

void nanoOS_task_init(void *entry_point_ptr, Task *task_ptr, uint8_t priority)
{
	static uint8_t task_index = 0; //el id sera correlativo a medida que se generen mas tareas

	/*
	 * Al principio se efectua un pequeño checkeo para determinar si llegamos a
	 * la cantidad maxima de tareas que pueden definirse para este OS. En el
	 * caso de que se traten de inicializar mas tareas que el numero maximo
	 * soportado, se guarda un codigo de error en la estructura de control del
	 * OS y la tarea no se inicializa. y la tarea no se inicializa. La tarea
	 * idle debe ser exceptuada del conteo de cantidad maxima de tareas porque
	 * si al momento de iniciar el sistema ya se definieron la cantidad maxima
	 * de tareas posible, la tarea idle seria la numero 9 y la primer condicion
	 * es falsa.
	 */

	if (nanoOS_control.tasks_counter < MAX_TASK_COUNT)
	{
		task_ptr->stack[STACK_SIZE / 4 - XPSR] = INIT_XPSR;					  //necesario para bit thumb
		task_ptr->stack[STACK_SIZE / 4 - PC_REG] = (uint32_t)entry_point_ptr; //direccion de la tarea (ENTRY_POINT)

		/*
		 * El valor previo de LR (que es EXEC_RETURN en este caso) es necesario
		 * dado que en esta implementacion, se llama a una funcion desde dentro
		 * del handler de PendSV con lo que el valor de LR se modifica por la
		 * direccion de retorno para cuando se termina de ejecutar
		 * getContextoSiguiente.
		 */
		task_ptr->stack[STACK_SIZE / 4 - LR_PREV_VALUE] = EXEC_RETURN;

		task_ptr->stack_pointer = (uintptr_t)(task_ptr->stack + STACK_SIZE / 4 - FULL_STACKING_SIZE);

		/*
		 * En esta seccion se guarda el entry point de la tarea, se le asigna id
		 * a la misma y se pone la misma en estado READY. Todas las tareas se
		 * crean en estado READY.
		 */
		task_ptr->entry_point = entry_point_ptr;
		task_ptr->stack[STACK_SIZE / 4 - LR] = (uint32_t)&return_hook; //Retorno de la tarea (no deberia darse)
		task_ptr->index = task_index;
		task_ptr->state = TASK_READY;
		task_ptr->priority = priority;

		/*
		 * Actualizacion de la estructura de control del OS, guardando el
		 * puntero a la estructura de tarea que se acaba de inicializar, y se
		 * actualiza la cantidad de tareas definidas en el sistema. Luego se
		 * incrementa el contador de id, dado que se le otorga un id correlativo
		 * a cada tarea inicializada, segun el orden en que se inicializan.
		 */
		nanoOS_control.tasks_list[task_index] = task_ptr;
		nanoOS_control.tasks_counter++;
		nanoOS_control.task_priority_counter[priority]++;

		task_index++;
	}
	else
	{
		/*
		 * En el caso que se hayan excedido la cantidad de tareas que se pueden definir, se actualiza
		 * el ultimo error generado en la estructura de control del OS y se llama a errorHook y se
		 * envia informacion de quien es quien la invoca.
		 */
		nanoOS_control.error = ERR_OS_CANT_TAREAS;
		error_hook(&nanoOS_task_init);
	}
}

void nanoOS_init(void)
{
	/*
	 * Todas las interrupciones tienen prioridad 0 (la maxima) al iniciar la ejecucion. Para que
	 * no se de la condicion de fault mencionada en la teoria, debemos bajar su prioridad en el
	 * NVIC. La cuenta matematica que se observa da la probabilidad mas baja posible.
	 */
	NVIC_SetPriority(PendSV_IRQn, (1 << __NVIC_PRIO_BITS) - 1);

	/*
	 * Es necesaria la inicializacion de la tarea idle, la cual no es visible al usuario
	 * El usuario puede eventualmente poblarla de codigo o redefinirla, pero no debe
	 * inicializarla ni definir una estructura para la misma.
	 */
	init_idle_task();

	/*
	 * Al iniciar el OS se especifica que se encuentra en la primer ejecucion desde un reset.
	 * Este estado es util para cuando se debe ejecutar el primer cambio de contexto. Los
	 * punteros de tarea_actual y tarea_siguiente solo pueden ser determinados por el scheduler
	 */
	nanoOS_control.system_state = NANOOS_FROM_RESET;
	nanoOS_control.current_task = NULL;
	nanoOS_control.next_task = NULL;

	/*
	 * El vector de tareas termina de inicializarse asignando NULL a las
	 * posiciones que estan luego de la ultima tarea. Esta situacion se da
	 * cuando se definen menos de 8 tareas. Estrictamente no existe necesidad de
	 * esto, solo es por seguridad.     
	 * NOTA: La ultima tarea de este vector sera siempre la tarea idle
	 */

	for (uint8_t i = 0; i < MAX_TASK_COUNT; i++)
	{
		if (i >= nanoOS_control.tasks_counter)
			nanoOS_control.tasks_list[i] = NULL;
	}

	priority_sort();
}

void SysTick_Handler(void)
{
	/*
	 * Systick se encarga de actualizar todos los temporizadores por lo que se recorren
	 * todas las tareas que esten definidas y si tienen un valor de ticks de bloqueo mayor
	 * a cero, se decrementan en una unidad.
	 */
	uint8_t i = 0;
	Task *task; //variable para legibilidad

	while (nanoOS_control.tasks_list[i] != NULL)
	{
		task = nanoOS_control.tasks_list[i];
		if (task->blocked_ticks > 0 && --task->blocked_ticks == 0 && task->state == TASK_BLOCKED)
		{
			task->state = TASK_READY;
		}
		i++;
	}

	/*
	 * Dentro del SysTick handler se llama al scheduler. Separar el scheduler de
	 * getContextoSiguiente da libertad para cambiar la politica de scheduling en cualquier
	 * estadio de desarrollo del OS. Recordar que scheduler() debe ser lo mas corto posible
	 */

	scheduler();

	/*
	 * Luego de determinar cual es la tarea siguiente segun el scheduler, se ejecuta la funcion
	 * tickhook.
	 */

	tick_hook();
}

void nanoOS_CPU_yield(void)
{
	scheduler();
}

static void setPendSV(void)
{
	/*
	 * Se indica en la estructura del OS que el cambio de contexto se esta por
	 * invocar Se hace antes de setear PendSV para no interferir con las
	 * barreras de datos y memoria
	 */
	nanoOS_control.is_context_switch_needed = false;

	/**
	 * Se setea el bit correspondiente a la excepcion PendSV
	 */
	SCB->ICSR = SCB_ICSR_PENDSVSET_Msk;

	/**
	 * Instruction Synchronization Barrier; flushes the pipeline and ensures
	 * that all previous instructions are completed before executing new
	 * instructions
	 */
#ifndef TEST
	__ISB();
#endif

	/**
	 * Data Synchronization Barrier; ensures that all memory accesses are
	 * completed before next instruction is executed
	 */
#ifndef TEST
	__DSB();
#endif
}

uint32_t getContextoSiguiente(uint32_t sp_current)
{
	uintptr_t sp_next;
	/*
	 * Esta funcion efectua el cambio de contexto. Se guarda el MSP (sp_current)
	 * en la variable correspondiente de la estructura de la tarea corriendo
	 * actualmente. Ahora que el estado BLOCKED esta implementado, se debe hacer
	 * un assert de si la tarea actual fue expropiada mientras estaba corriendo
	 * o si la expropiacion fue hecha de manera prematura dado que paso a estado
	 * BLOCKED. En el segundo caso, solamente se puede pasar de BLOCKED a READY
	 * a partir de un evento. Se carga en la variable sp_next el stack
	 * pointer de la tarea siguiente, que fue definida por el scheduler. Se
	 * actualiza la misma a estado RUNNING y se retorna al handler de PendSV
	 */
	nanoOS_control.current_task->stack_pointer = sp_current;

	/*
	 * Esta funcion efectua el cambio de contexto. Se guarda el MSP (sp_current)
	 * en la variable correspondiente de la estructura de la tarea corriendo
	 * actualmente. Ahora que el estado BLOCKED esta implementado, se debe hacer
	 * un assert de si la tarea actual fue expropiada mientras estaba corriendo
	 * o si la expropiacion fue hecha de manera prematura dado que paso a estado
	 * BLOCKED. En el segundo caso, solamente se puede pasar de BLOCKED a READY
	 * a partir de un evento. Se carga en la variable sp_next el stack
	 * pointer de la tarea siguiente, que fue definida por el scheduler. Se
	 * actualiza la misma a estado RUNNING y se retorna al handler de PendSV
	 */

	nanoOS_control.current_task->stack_pointer = sp_current;
	if (nanoOS_control.current_task->state == TASK_RUNNING)
	{
		nanoOS_control.current_task->state = TASK_READY;
	}

	sp_next = nanoOS_control.next_task->stack_pointer;

	nanoOS_control.current_task = nanoOS_control.next_task;
	nanoOS_control.current_task->state = TASK_RUNNING;

	/*
	 * Indicamos que luego de retornar de esta funcion, ya no es necesario un
	 * cambio de contexto porque se acaba de gestionar.
	 */
	nanoOS_control.system_state = NANOOS_NORMAL_RUN;

	return sp_next;
}

void nanoOS_delay(uint32_t ticks)
{
	Task *current_task;

	/*
	 * En esta version se modifica la funcion delay en algunos aspectos:
	 * 1) Ya no es necesario corroborar que la funcion este en RUNNING al
	 *    momento de cargar los ticks dado que la obtencion del puntero a la
	 *    estructura de la tarea y la asignacion de ticks se hace dentro de una
	 *    seccion critica
	 * 2) Nada del codigo se ejecuta si la variable ticks vale 0
	 */

	if (ticks > 0)
	{
		nanoOS_critical_enter();
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		current_task = nanoOS_control.current_task;
		current_task->blocked_ticks = ticks;
		//----------------------------------------------------------------------------------------------------
		nanoOS_critical_exit();

		/*
		 * El proximo bloque while tiene la finalidad de asegurarse que la tarea
		 * solo se desbloquee en el momento que termine la cuenta de ticks. Si
		 * por alguna razon la tarea se vuelve a ejecutar antes que termine el
		 * periodo de bloqueado, queda atrapada. La bandera delay activo es
		 * puesta en false por el SysTick. En teoria esto no deberia volver a
		 * ejecutarse dado que el scheduler no vuelve a darle CPU hasta que no
		 * pase a estado READY
		 */

		while (current_task->blocked_ticks > 0)
		{
			current_task->state = TASK_BLOCKED;
			nanoOS_CPU_yield();
		}
	}
}

void __attribute__((weak, noreturn)) return_hook(void)
{
	while (1)
		;
}

void __attribute__((weak)) tick_hook(void)
{
	// Leave empty to let the compiler optimize it.
}

void __attribute__((weak, noreturn)) error_hook(__attribute__((unused)) const void *caller)
{
	/*
	 * Revisar el contenido de nanoOS_control.error para obtener informacion.
	 */
	while (1)
		;
}

void __attribute__((weak, noreturn)) idle_task(void)
{
	while (1)
	{
		__NOP();
	}
}

void nanoOS_semaphore_init(Semaphore *sem)
{
	sem->is_taken = true;
	sem->associated_task = NULL;
}

void nanoOS_semaphore_take(Semaphore *sem)
{
	bool Salir = false;
	Task *current_task;

	/*
	 * En el caso de que otra tarea desbloquee por error la tarea que se acaba
	 * de bloquear con el semaforo (en el caso que este tomado) el bloque while
	 * se encarga de volver a bloquearla hasta tanto no se haga un give
	 */
	while (!Salir)
	{
		/*
		 * Si el semaforo esta tomado, la tarea actual debe bloquearse y se
		 * mantiene un puntero a la estructura de la tarea actual, la que recibe
		 * el nombre de tarea asociada. Luego se hace un CPU yield dado que no
		 * se necesita mas el CPU hasta que se libere el semaforo.
		 *
		 * Si el semaforo estaba libre, solamente se marca como tomado y se
		 * retorna.
		 *
		 * Se agrega una seccion critica al momento de obtener la tarea actual e
		 * interactuar con su estado
		 */
		if (sem->is_taken)
		{

			nanoOS_critical_enter();
			//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			current_task = nanoOS_control.current_task;
			current_task->state = TASK_BLOCKED;
			sem->associated_task = current_task;
			//---------------------------------------------------------------------------
			nanoOS_critical_exit();
			nanoOS_CPU_yield();
		}
		else
		{
			sem->is_taken = true;
			Salir = true;
		}
	}
}

void nanoOS_semaphore_give(Semaphore *sem)
{
	/*
	 * Por seguridad, se deben hacer dos checkeos antes de hacer un give sobre
	 * el semaforo. En el caso de que se ambas condiciones sean verdaderas, se
	 * libera y se actualiza la tarea correspondiente a estado ready.
	 */

	if (sem->is_taken == true && sem->associated_task != NULL)
	{
		sem->is_taken = false;
		sem->associated_task->state = TASK_READY;

		/*
		 * Si es llamada desde una interrupcion, se debe indicar que es
		 * necesario efectuar un scheduling, porque seguramente existe una tarea
		 * esperando este evento
		 */
		if (nanoOS_control.system_state == NANOOS_IRQ_RUN)
			nanoOS_control.scheduling_from_IRQ = true;
	}
}

void nanoOS_queue_init(Queue *queue, uint16_t datasize)
{
	queue->head_idx = 0;
	queue->tail_idx = 0;
	queue->associated_task = NULL;
	queue->element_size = datasize;
}

void nanoOS_queue_write(Queue *queue, void *data)
{
	uint16_t head_idx;		 //variable para legibilidad
	uint16_t elements_count; //variable para legibilidad
	Task *current_task;

	head_idx = queue->head_idx * queue->element_size;
	elements_count = QUEUE_HEAP_SIZE / queue->element_size;

	/*
	 * el primer bloque determina tres cosas, que gracias a la disposicion de
	 * los parentesis se dan en un orden especifico:
	 * 1) Se determina si head == tail, con lo que la cola estaria vacia
	 * 2) Sobre el resultado de 1) se determina si existe una tarea asociada
	 * 3) Si la cola esta vacia, y el puntero a la tarea asociada es valido, se
	 *      verifica si la tarea asociada esta bloqueada Estas condiciones en
	 *      ese orden determinan si se trato de leer de una cola vacia y la
	 *      tarea que quizo leer se bloqueo porque la misma estaba vacia. Como
	 *      seguramente en este punto se escribe un dato a la misma, esa tarea
	 *      tiene que pasar a ready
	 */

	if (((queue->head_idx == queue->tail_idx) && queue->associated_task != NULL) &&
		queue->associated_task->state == TASK_BLOCKED)
	{
		queue->associated_task->state = TASK_READY;
		/*
		 * Si es llamada desde una interrupcion, se debe indicar que es
		 * necesario efectuar un scheduling, porque seguramente existe una tarea
		 * esperando este evento
		 */
		if (nanoOS_control.system_state == NANOOS_IRQ_RUN)
		{
			nanoOS_control.scheduling_from_IRQ = true;
		}
	}
	/*
	* En el caso de que se quiera escribir una cola desde un ISR y este llena,
	* la operacion es abortada (no se puede bloquear un handler)
	*/
	if (nanoOS_control.system_state == NANOOS_IRQ_RUN &&
		(queue->head_idx + 1) % elements_count == queue->tail_idx)
	{
		// os_setWarning(WARN_OS_QUEUE_FULL_ISR);
		return; //operacion abortada
	}

	/*
	* El siguiente bloque while determina que hasta que la cola no tenga lugar
	* disponible, no se avance. Si no tiene lugar se bloquea la tarea actual
	* que es la que esta tratando de escribir y luego se hace un yield
	*/
	while ((queue->head_idx + 1) % elements_count == queue->tail_idx)
	{

		nanoOS_critical_enter();
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		current_task = nanoOS_control.current_task;
		current_task->state = TASK_BLOCKED;
		queue->associated_task = current_task;
		//---------------------------------------------------------------------------
		nanoOS_critical_exit();
		nanoOS_CPU_yield();
	}

	/*
	* Si la cola tiene lugar, se escribe mediante la funcion memcpy que copia un
	* bloque completo de memoria iniciando desde la direccion apuntada por el
	* primer elemento. Como data es un vector del tipo uint8_t, la aritmetica de
	* punteros es byte a byte (consecutivos) y se logra el efecto deseado Esto
	* permite guardar datos definidos por el usuario, como ser estructuras de
	* datos completas. Luego se actualiza el undice head y se limpia la tarea
	* asociada, dado que ese puntero ya no tiene utilidad
	*/
	memcpy(queue->data + head_idx, data, queue->element_size);
	queue->head_idx = (queue->head_idx + 1) % elements_count;
	queue->associated_task = NULL;
}

void nanoOS_queue_read(Queue *queue, void *data)
{
	uint16_t elements_count; //variable para legibilidad
	uint16_t tail_idx;		 //variable para legibilidad
	Task *current_task;

	tail_idx = queue->tail_idx * queue->element_size;
	elements_count = QUEUE_HEAP_SIZE / queue->element_size;

	/*
	* el primer bloque determina tres cosas, que gracias a la disposicion de los
	* parentesis se dan en un orden especifico:
	* 1) Se determina si la cola esta llena (head+1)%CANT_ELEMENTOS == tail
	* 2) Sobre el resultado de 1) se determina si existe una tarea asociada
	* 3) Si la cola esta llena, y el puntero a la tarea asociada es valido, se
	*      verifica si la tarea asociada esta bloqueada Estas condiciones en ese
	*      orden determinan si se trato de escribir en una cola llena y la tarea
	*      que quizo escribir se bloqueo porque la misma estaba llena. Como
	*      seguramente en este punto se lee un dato de la misma, esa tarea tiene
	*      que pasar a ready
	*/

	if ((((queue->head_idx + 1) % elements_count == queue->tail_idx) &&
		 queue->associated_task != NULL) &&
		queue->associated_task->state == TASK_BLOCKED)
	{
		queue->associated_task->state = TASK_READY;
		/*
		 * Si es llamada desde una interrupcion, se debe indicar que es
		 * necesario efectuar un scheduling, porque seguramente existe una tarea
		 * esperando este evento
		 */
		if (nanoOS_control.system_state == NANOOS_IRQ_RUN)
		{
			nanoOS_control.scheduling_from_IRQ = true;
		}
	}
	/*
	 * En el caso de que se quiera leer una cola desde un ISR y este vacia, la
	 * operacion es abortada (no se puede bloquear un handler)
	 */
	if (nanoOS_control.system_state == NANOOS_IRQ_RUN && queue->head_idx == queue->tail_idx)
	{
		// os_setWarning(WARN_OS_QUEUE_EMPTY_ISR);
		return; //operacion abortada
	}

	/*
	 * El siguiente bloque while determina que hasta que la cola no tenga un
	 * dato disponible, no se avance. Si no hay un dato que leer, se bloquea la
	 * tarea actual que es la que esta tratando de leer un dato y luego se hace
	 * un yield
	 */
	while (queue->head_idx == queue->tail_idx)
	{
		nanoOS_critical_enter();
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		current_task = nanoOS_control.current_task;
		current_task->state = TASK_BLOCKED;
		queue->associated_task = current_task;
		//---------------------------------------------------------------------------
		nanoOS_critical_exit();
		nanoOS_CPU_yield();
	}

	/*
	* Si la cola tiene datos, se lee mediante la funcion memcpy que copia un
	* bloque completo de memoria iniciando desde la direccion apuntada por el
	* primer elemento. Como data es un vector del tipo uint8_t, la aritmetica de
	* punteros es byte a byte (consecutivos) y se logra el efecto deseado Esto
	* permite guardar datos definidos por el usuario, como ser estructuras de
	* datos completas. Luego se actualiza el undice head y se limpia la tarea
	* asociada, dado que ese puntero ya no tiene utilidad
	*/
	memcpy(data, queue->data + tail_idx, queue->element_size);
	queue->tail_idx = (queue->tail_idx + 1) % elements_count;
	queue->associated_task = NULL;
}

void nanoOS_critical_enter()
{
	__disable_irq();
	nanoOS_control.critical_counter++;
}

void nanoOS_critical_exit()
{
	if (--nanoOS_control.critical_counter <= 0)
	{
		nanoOS_control.critical_counter = 0;
		__enable_irq();
	}
}

static void init_idle_task(void)
{
	idle.stack[STACK_SIZE / 4 - XPSR] = INIT_XPSR;				//necesario para bit thumb
	idle.stack[STACK_SIZE / 4 - PC_REG] = (uint32_t)&idle_task; //direccion de la tarea (ENTRY_POINT)
	idle.stack[STACK_SIZE / 4 - LR] = (uint32_t)&return_hook;	//Retorno de la tarea (no deberia darse)

	idle.stack[STACK_SIZE / 4 - LR_PREV_VALUE] = EXEC_RETURN;
	idle.stack_pointer = (uint32_t)(idle.stack + STACK_SIZE / 4 - FULL_STACKING_SIZE);

	idle.entry_point = &idle_task;
	idle.index = 0xFF;
	idle.state = TASK_READY;
	idle.priority = 0xFF;
}

static void scheduler(void)
{
	static uint8_t priority_idx[PRIORITY_COUNT]; //indice de tareas a ejecutar segun prioridad
	uint8_t task_array_idx = 0;
	uint8_t current_priority = MAX_PRIORITY; //Maxima prioridad al iniciar
	uint8_t curr_priority_blk_tasks = 0;
	bool salir = false;
	uint8_t blocked_counter = 0;

	/*
	 * El scheduler recibe la informacion desde la variable estado_sistema si es
	 * el primer ingreso desde el ultimo reset. Si esto es asi, se carga como
	 * tarea actual la tarea idle. Esto es mas prolijo que tener una bandera
	 * para cada situacion. Es necesario que el vector que lleva los indices de
	 * las tareas este a cero antes de comenzar lo que se logra con la funcion
	 * memset()
	 */
	if (nanoOS_control.system_state == NANOOS_FROM_RESET)
	{
		nanoOS_control.current_task = &idle;
		nanoOS_control.is_context_switch_needed = true;
		memset(priority_idx, 0, sizeof(uint8_t) * PRIORITY_COUNT);
		nanoOS_control.system_state = NANOOS_NORMAL_RUN;
	}
	else
	{
		/*
		* Se comienza a iterar sobre el vector de tareas, pero teniendo en
		* cuenta las prioridades de las tareas. En esta implementacion, durante
		* la ejecucion de os_Init() se aplica la tecnica de quicksort sobre el
		* vector que contiene la lista de tareas y se ordenan los punteros a
		* tareas segun la prioridad que tengan. Los punteros a tareas quedan
		* ordenados de mayor a menor prioridad. Gracias a eso, dividiendo el
		* vector de punteros en cuantas prioridades haya podemos recorrer estas
		* subsecciones manteniendo indices para cada prioridad
		*
		* La mecanica de RoundRobin para tareas de igual prioridad se mantiene,
		* asimismo la determinacion de cuando la tarea idle debe ser ejecutada.
		* Cuando se recorren todas las tareas de una prioridad y todas estan
		* bloqueadas, entonces se pasa a la prioridad menor siguiente
		*
		* Recordar que aunque todas las tareas definidas por el usuario esten
		* bloqueadas la tarea Idle solamente puede tomar estados READY y
		* RUNNING.
		*/

		/*
		* Puede darse el caso en que se haya invocado la funcion os_CpuYield()
		* la cual hace una llamada al scheduler. Si durante la ejecucion del
		* scheduler (la cual fue forzada) y esta siendo atendida en modo Thread
		* ocurre una excepcion dada por el SysTick, habra una instancia del
		* scheduler pendiente en modo trhead y otra corriendo en modo Handler
		* invocada por el SysTick. Para evitar un doble scheduling, se controla
		* que el sistema no este haciendo uno ya. En caso afirmativo se vuelve
		* prematuramente
		*/
		if (nanoOS_control.system_state == NANOOS_SCHEDULING)
		{
			return;
		}

		/*
		* Cambia el estado del sistema para que no se produzcan schedulings
		* anidados cuando existen forzados por alguna API del sistema.
		*/
		nanoOS_control.system_state = NANOOS_SCHEDULING;

		/*
		* Como la determinacion de cuantas funciones estan en READY o BLOCKED es
		* dinamica, se hace un bucle con un testigo para salir de el
		*/
		while (!salir)
		{
			/*
			* La variable indiceArrayTareas contiene la posicion real dentro del
			* array listaTareas de la tarea siguiente a ser ejecutada. Se
			* inicializa en 0
			*/
			task_array_idx = 0;

			/*
			* Puede darse el caso de que se hayan definido tareas de prioridades no
			* contiguas, por ejemplo dos tareas de maxima prioridad y una de minima
			* prioridad. Quiere decir que no existen tareas con prioridades entre
			* estas dos. Este bloque if determina si existen funciones para la
			* prioridad actual. Si no existen, se pasa a la prioridad menor
			* siguiente
			*/

			if (nanoOS_control.task_priority_counter[current_priority] > 0)
			{
				/*
				 * esta linea asegura que el indice siempre este dentro de los
				 * limites de la subseccion que forman los punteros a las tareas
				 * de prioridad actual
				 */
				priority_idx[current_priority] %=
					nanoOS_control.task_priority_counter[current_priority];
				/*
				 * El bucle for hace una conversion de indices de subsecciones
				 * al indice real que debe usarse sobre el vector de punteros a
				 * tareas. Cuando se baja de prioridad debe sumarse el total de
				 * tareas que existen en la subseccion anterior. Recordar que el
				 * vector de tareas esta ordenado de mayor a menor
				 */
				for (uint8_t i = 0; i < current_priority; i++)
				{
					task_array_idx += nanoOS_control.task_priority_counter[i];
				}
				task_array_idx += priority_idx[current_priority];

				switch (nanoOS_control.tasks_list[task_array_idx]->state)
				{

				case TASK_READY:
					nanoOS_control.next_task = nanoOS_control.tasks_list[task_array_idx];
					nanoOS_control.is_context_switch_needed = true;
					priority_idx[current_priority]++;
					salir = true;
					break;

				case TASK_BLOCKED:
					/*
				* Para el caso de las tareas bloqueadas, la variable
				* cantBloqueadas_prioActual se utiliza para hacer el seguimiento de
				* si se debe bajar un escalon de prioridad El bucle del scheduler se
				* ejecuta completo, pasando por todas las prioridades cada vez hasta
				* que encuentra una tarea en READY. La determinacion de la necesidad
				* de ejecucion de la tarea idle sigue siendo la misma.
				*/
					blocked_counter++;
					curr_priority_blk_tasks++;
					priority_idx[current_priority]++;
					if (blocked_counter >= nanoOS_control.tasks_counter)
					{
						nanoOS_control.next_task = &idle;
						nanoOS_control.is_context_switch_needed = true;
						salir = true;
					}
					else
					{
						if (curr_priority_blk_tasks == nanoOS_control.task_priority_counter[current_priority])
						{
							curr_priority_blk_tasks = 0;
							priority_idx[current_priority] = 0;
							current_priority++;
						}
					}
					break;
				case TASK_RUNNING:
					/*
				* El unico caso que la siguiente tarea este en estado RUNNING es
				* que todas las demas tareas excepto la tarea corriendo actualmente
				* esten en estado BLOCKED, con lo que un cambio de contexto no es
				* necesario, porque se sigue ejecutando la misma tarea
				*/
					nanoOS_control.is_context_switch_needed = false;
					salir = true;
					break;
				default:
					/*
				* En el caso que lleguemos al caso default, la tarea tomo un estado
				* el cual es invalido, por lo que directamente se llama errorHook y
				* se actualiza la variable de ultimo error
				*/
					nanoOS_control.error = ERR_OS_SCHEDULING;
					error_hook(&scheduler);
				}
			}
			else
			{
				priority_idx[current_priority] = 0;
				current_priority++;
			}
		}

		/*
	 * Antes de salir del scheduler se devuelve el sistema a su estado normal
	 */
		nanoOS_control.system_state = NANOOS_NORMAL_RUN;

		/*
	 * Se checkea la bandera correspondiente para verificar si es necesario un cambio de
	 * contexto. En caso afirmativo, se lanza PendSV
	 */

		if (nanoOS_control.is_context_switch_needed)
		{
			setPendSV();
		}
	}
}

static void priority_sort(void)
{
	// Create an auxiliary stack
	int32_t stack[MAX_TASK_COUNT];

	// initialize top of stack
	int32_t top = -1;
	int32_t l = 0;
	int32_t h = nanoOS_control.tasks_counter - 1;

	// push initial values of l and h to stack (indices a estructuras de tareas)
	stack[++top] = l;
	stack[++top] = h;

	// Keep popping from stack while is not empty
	while (top >= 0)
	{
		// Pop h and l
		h = stack[top--];
		l = stack[top--];

		// Set pivot element at its correct position
		// in sorted array
		int32_t p = partition(nanoOS_control.tasks_list, l, h);

		// If there are elements on left side of pivot,
		// then push left side to stack
		if (p - 1 > l)
		{
			stack[++top] = l;
			stack[++top] = p - 1;
		}

		// If there are elements on right side of pivot,
		// then push right side to stack
		if (p + 1 < h)
		{
			stack[++top] = p + 1;
			stack[++top] = h;
		}
	}
}

static int32_t partition(Task **arr, int32_t l, int32_t h)
{
	const Task *x = arr[h];
	Task *aux;
	int32_t i = (l - 1);

	for (int j = l; j <= h - 1; j++)
	{
		if (arr[j]->priority <= x->priority)
		{
			i++;
			aux = arr[i];
			arr[i] = arr[j];
			arr[j] = aux;
		}
	}
	aux = arr[i + 1];
	arr[i + 1] = arr[h];
	arr[h] = aux;

	return (i + 1);
}

bool nanoOS_add_IRQ(IRQn_Type irq, void *usr_isr)
{
	bool Ret = 0;

	/*
	 * Determinamos si la insterrupcion no fue definida anteriormente por el usuario
	 * Entonces cargamos el puntero de la funcion del usuario y  habilitamos esa interrupcion
	 * en el NVIC
	 */
	if (usr_isr_array[irq] == NULL)
	{
		usr_isr_array[irq] = usr_isr;
		NVIC_ClearPendingIRQ(irq);
		NVIC_EnableIRQ(irq);
		Ret = true;
	}

	return Ret;
}

bool nanoOS_remove_IRQ(IRQn_Type irq)
{
	bool Ret = 0;

	if (usr_isr_array[irq] != NULL)
	{
		usr_isr_array[irq] = NULL;
		NVIC_ClearPendingIRQ(irq);
		NVIC_DisableIRQ(irq);
		Ret = true;
	}

	return Ret;
}

/********************************************************************************
 * Esta funcion es la que todas las interrupciones llaman. Se encarga de llamar
 * a la funcion de usuario que haya sido cargada. LAS FUNCIONES DE USUARIO
 * LLAMADAS POR ESTA FUNCION SE EJECUTAN EN MODO HANDLER DE IGUAL FORMA. CUIDADO
 * CON LA CARGA DE CODIGO EN ELLAS, MISMAS REGLAS QUE EN BARE METAL
 *******************************************************************************/
static void nanoOS_IRQHandler(IRQn_Type IRQn)
{
	nanoOS_state previous_state;
	void (*user_irq_routine)(void);

	/*
	 * Guardamos el estado del sistema para restablecerlo al salir de
	 * la interrupcion. Este estado nos permite utilizar la misma api
	 * de sistema operativo para todos los casos
	 */
	previous_state = nanoOS_control.system_state;

	nanoOS_control.system_state = NANOOS_IRQ_RUN;

	/*
	 * Llamamos a la funcion definida por el usuario
	 */
	user_irq_routine = usr_isr_array[IRQn];
	user_irq_routine();

	/*
	 * Retomamos el estado anterior de sistema operativo
	 */
	nanoOS_control.system_state = previous_state;

	/*
	 * Debemos limpiar la interrupcion que acabamos de atender, sino se entra
	 * por siempre a la funcion de interrupcion
	 */
	NVIC_ClearPendingIRQ(IRQn);

	/*
	 * Si hubo alguna llamada desde una interrupcion a una api liberando un
	 * evento, entonces llamamos al scheduler
	 */
	if (nanoOS_control.scheduling_from_IRQ)
	{
		nanoOS_control.scheduling_from_IRQ = false;
		nanoOS_CPU_yield();
	}
}

void POWER_CLOCK_IRQHandler(void) { nanoOS_IRQHandler(POWER_CLOCK_IRQn); }
void RADIO_IRQHandler(void) { nanoOS_IRQHandler(RADIO_IRQn); }
void UARTE0_UART0_IRQHandler(void) { nanoOS_IRQHandler(UARTE0_UART0_IRQn); }
void SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQHandler(void) { nanoOS_IRQHandler(SPIM0_SPIS0_TWIM0_TWIS0_SPI0_TWI0_IRQn); }
void SPIM1_SPIS1_TWIM1_TWIS1_SPI1_TWI1_IRQHandler(void) { nanoOS_IRQHandler(SPIM1_SPIS1_TWIM1_TWIS1_SPI1_TWI1_IRQn); }
void NFCT_IRQHandler(void) { nanoOS_IRQHandler(NFCT_IRQn); }
void GPIOTE_IRQHandler(void) { nanoOS_IRQHandler(GPIOTE_IRQn); }
void SAADC_IRQHandler(void) { nanoOS_IRQHandler(SAADC_IRQn); }
void TIMER0_IRQHandler(void) { nanoOS_IRQHandler(TIMER0_IRQn); }
void TIMER1_IRQHandler(void) { nanoOS_IRQHandler(TIMER1_IRQn); }
void TIMER2_IRQHandler(void) { nanoOS_IRQHandler(TIMER2_IRQn); }
void RTC0_IRQHandler(void) { nanoOS_IRQHandler(RTC0_IRQn); }
void TEMP_IRQHandler(void) { nanoOS_IRQHandler(TEMP_IRQn); }
void RNG_IRQHandler(void) { nanoOS_IRQHandler(RNG_IRQn); }
void ECB_IRQHandler(void) { nanoOS_IRQHandler(ECB_IRQn); }
void CCM_AAR_IRQHandler(void) { nanoOS_IRQHandler(CCM_AAR_IRQn); }
void WDT_IRQHandler(void) { nanoOS_IRQHandler(WDT_IRQn); }
void RTC1_IRQHandler(void) { nanoOS_IRQHandler(RTC1_IRQn); }
void QDEC_IRQHandler(void) { nanoOS_IRQHandler(QDEC_IRQn); }
void COMP_LPCOMP_IRQHandler(void) { nanoOS_IRQHandler(COMP_LPCOMP_IRQn); }
void SWI0_EGU0_IRQHandler(void) { nanoOS_IRQHandler(SWI0_EGU0_IRQn); }
void SWI1_EGU1_IRQHandler(void) { nanoOS_IRQHandler(SWI1_EGU1_IRQn); }
void SWI2_EGU2_IRQHandler(void) { nanoOS_IRQHandler(SWI2_EGU2_IRQn); }
void SWI3_EGU3_IRQHandler(void) { nanoOS_IRQHandler(SWI3_EGU3_IRQn); }
void SWI4_EGU4_IRQHandler(void) { nanoOS_IRQHandler(SWI4_EGU4_IRQn); }
void SWI5_EGU5_IRQHandler(void) { nanoOS_IRQHandler(SWI5_EGU5_IRQn); }
void TIMER3_IRQHandler(void) { nanoOS_IRQHandler(TIMER3_IRQn); }
void TIMER4_IRQHandler(void) { nanoOS_IRQHandler(TIMER4_IRQn); }
void PWM0_IRQHandler(void) { nanoOS_IRQHandler(PWM0_IRQn); }
void PDM_IRQHandler(void) { nanoOS_IRQHandler(PDM_IRQn); }
void MWU_IRQHandler(void) { nanoOS_IRQHandler(MWU_IRQn); }
void PWM1_IRQHandler(void) { nanoOS_IRQHandler(PWM1_IRQn); }
void PWM2_IRQHandler(void) { nanoOS_IRQHandler(PWM2_IRQn); }
void SPIM2_SPIS2_SPI2_IRQHandler(void) { nanoOS_IRQHandler(SPIM2_SPIS2_SPI2_IRQn); }
void RTC2_IRQHandler(void) { nanoOS_IRQHandler(RTC2_IRQn); }
void I2S_IRQHandler(void) { nanoOS_IRQHandler(I2S_IRQn); }
void FPU_IRQHandler(void) { nanoOS_IRQHandler(FPU_IRQn); }

/*==================[end of file]============================================*/
