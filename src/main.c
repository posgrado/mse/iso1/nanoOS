/*==================[inclusions]=============================================*/

#include "main.h"
#include "nanoOS.h"
#include "uart.h"
#include "boards.h"
#include "nrf_drv_gpiote.h"
#include "nrf_drv_uart.h"


/*==================[macros and definitions]=================================*/

/*==================[Global data declaration]==============================*/

Task fsm_task; //Stack Pointer para finite state machine

Semaphore button_pressed;

typedef enum
{
	FSM_IDLE,
	FSM_WAIT_BTN_1,
	FSM_WAIT_BTN_2,
	FSM_WAIT_RELEASE_1,
	FSM_WAIT_RELEASE_2,
	FSM_WAIT_RELEASE_1_1,
	FSM_WAIT_RELEASE_1_2,
	FSM_WAIT_RELEASE_2_1,
	FSM_WAIT_RELEASE_2_2,
	FSM_TURN_LED_0_ON,
	FSM_TURN_LED_0_OFF,
	FSM_TURN_LED_1_ON,
	FSM_TURN_LED_1_OFF,
	FSM_TURN_LED_2_ON,
	FSM_TURN_LED_2_OFF,
	FSM_TURN_LED_3_ON,
	FSM_TURN_LED_3_OFF,
} FSM_states;

FSM_states fsm_state = FSM_IDLE;

typedef enum
{
	BTN_1_PRESSED,
	BTN_1_RELEASED,
	BTN_2_PRESSED,
	BTN_2_RELEASED,
} Button_events;

Button_events btn_event;

bool counting_t1;
bool counting_t2;

uint32_t t1;
uint32_t t2;

/*==================[Definicion de tareas para el OS]==========================*/
__attribute__((noreturn)) void fsm(void)
{
	while (1)
	{
		switch (fsm_state)
		{
		case FSM_IDLE:
			t1 = 0;
			t2 = 0;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_1_PRESSED)
			{
				fsm_state = FSM_WAIT_BTN_2;
			}
			else if (btn_event == BTN_2_PRESSED)
			{
				fsm_state = FSM_WAIT_BTN_1;
			}
			else
			{
				fsm_state = FSM_IDLE;
			}
			break;
		case FSM_WAIT_BTN_2:
			counting_t1 = true;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_1_PRESSED)
			{
				fsm_state = FSM_IDLE;
			}
			else if (btn_event == BTN_2_PRESSED)
			{
				fsm_state = FSM_WAIT_RELEASE_1;
			}
			else
			{
				fsm_state = FSM_IDLE;
			}
			break;
		case FSM_WAIT_BTN_1:
			counting_t1 = true;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_2_PRESSED)
			{
				fsm_state = FSM_IDLE;
			}
			else if (btn_event == BTN_1_PRESSED)
			{
				fsm_state = FSM_WAIT_RELEASE_2;
			}
			else
			{
				fsm_state = FSM_IDLE;
			}
			break;
		case FSM_WAIT_RELEASE_1:
			counting_t1 = false;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_1_RELEASED)
			{
				fsm_state = FSM_WAIT_RELEASE_1_2;
			}
			else if (btn_event == BTN_2_RELEASED)
			{
				fsm_state = FSM_WAIT_RELEASE_1_1;
			}
			break;
		case FSM_WAIT_RELEASE_2:
			counting_t1 = false;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_1_RELEASED)
			{
				fsm_state = FSM_WAIT_RELEASE_2_2;
			}
			else if (btn_event == BTN_2_RELEASED)
			{
				fsm_state = FSM_WAIT_RELEASE_2_1;
			}
			break;
		case FSM_WAIT_RELEASE_1_1:
			counting_t2 = true;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_1_RELEASED)
			{
				fsm_state = FSM_TURN_LED_1_ON;
				counting_t2 = false;
			}
			break;
		case FSM_WAIT_RELEASE_1_2:
			counting_t2 = true;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_2_RELEASED)
			{
				fsm_state = FSM_TURN_LED_0_ON;
				counting_t2 = false;
			}
			break;
		case FSM_WAIT_RELEASE_2_1:
			counting_t2 = true;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_1_RELEASED)
			{
				fsm_state = FSM_TURN_LED_2_ON;
				counting_t2 = false;
			}
			break;
		case FSM_WAIT_RELEASE_2_2:
			counting_t2 = true;
			nanoOS_semaphore_take(&button_pressed);
			if (btn_event == BTN_2_RELEASED)
			{
				fsm_state = FSM_TURN_LED_3_ON;
				counting_t2 = false;
			}
			break;
		case FSM_TURN_LED_0_ON:
			bsp_board_led_on(BSP_BOARD_LED_0);
			print_msg(0, t1, t2);
			nanoOS_delay(t1 + t2);
			fsm_state = FSM_TURN_LED_0_OFF;
			break;
		case FSM_TURN_LED_1_ON:
			bsp_board_led_on(BSP_BOARD_LED_1);
			print_msg(1, t1, t2);
			nanoOS_delay(t1 + t2);
			fsm_state = FSM_TURN_LED_1_OFF;
			break;
		case FSM_TURN_LED_2_ON:
			bsp_board_led_on(BSP_BOARD_LED_2);
			print_msg(2, t1, t2);
			nanoOS_delay(t1 + t2);
			fsm_state = FSM_TURN_LED_2_OFF;
			break;
		case FSM_TURN_LED_3_ON:
			bsp_board_led_on(BSP_BOARD_LED_3);
			print_msg(3, t1, t2);
			nanoOS_delay(t1 + t2);
			fsm_state = FSM_TURN_LED_3_OFF;
			break;
		case FSM_TURN_LED_0_OFF:
			bsp_board_led_off(BSP_BOARD_LED_0);
			fsm_state = FSM_IDLE;
			break;
		case FSM_TURN_LED_1_OFF:
			bsp_board_led_off(BSP_BOARD_LED_1);
			fsm_state = FSM_IDLE;
			break;
		case FSM_TURN_LED_2_OFF:
			bsp_board_led_off(BSP_BOARD_LED_2);
			fsm_state = FSM_IDLE;
			break;
		case FSM_TURN_LED_3_OFF:
			bsp_board_led_off(BSP_BOARD_LED_3);
			fsm_state = FSM_IDLE;
			break;

		default:
			fsm_state = FSM_IDLE;
			break;
		}
	}
}

void tick_hook()
{
	if (counting_t1)
	{
		t1++;
	}
	else if (counting_t2)
	{
		t2++;
	}
}

void button_irq(nrfx_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
	bool is_btn_1_pressed;
	bool is_btn_2_pressed;
	is_btn_1_pressed = bsp_board_button_state_get(0);
	is_btn_2_pressed = bsp_board_button_state_get(1);
	switch (pin)
	{
	case BUTTON_1:
		if (is_btn_1_pressed)
		{
			//BUTTON_1 Pressed
			btn_event = BTN_1_PRESSED;
			nanoOS_semaphore_give(&button_pressed);
		}
		else
		{
			//BUTTON_1 Released
			btn_event = BTN_1_RELEASED;
			nanoOS_semaphore_give(&button_pressed);
		}
		break;
	case BUTTON_2:
		if (is_btn_2_pressed)
		{
			//BUTTON_2 Pressed
			btn_event = BTN_2_PRESSED;
			nanoOS_semaphore_give(&button_pressed);
		}
		else
		{
			//BUTTON_2 Released
			btn_event = BTN_2_RELEASED;
			nanoOS_semaphore_give(&button_pressed);
		}
		break;
	}
}

void in_pin_handler()
{
	uint32_t status = 0;
	uint32_t input[GPIO_COUNT] = {0};

	/* collect status of all GPIOTE pin events. Processing is done once all are collected and cleared.*/
	uint32_t i;
	nrf_gpiote_events_t event = NRF_GPIOTE_EVENTS_IN_0;
	uint32_t mask = (uint32_t)NRF_GPIOTE_INT_IN0_MASK;

	for (i = 0; i < GPIOTE_CH_NUM; i++)
	{
		if (nrf_gpiote_event_is_set(event) && nrf_gpiote_int_is_enabled(mask))
		{
			nrf_gpiote_event_clear(event);
			status |= mask;
		}
		mask <<= 1;
		/* Incrementing to next event, utilizing the fact that events are grouped together
         * in ascending order. */
		event = (nrf_gpiote_events_t)((uint32_t)event + sizeof(uint32_t));
	}

	/* collect PORT status event, if event is set read pins state. Processing is postponed to the
     * end of interrupt. */
	if (nrf_gpiote_event_is_set(NRF_GPIOTE_EVENTS_PORT))
	{
		nrf_gpiote_event_clear(NRF_GPIOTE_EVENTS_PORT);
		status |= (uint32_t)NRF_GPIOTE_INT_PORT_MASK;
		nrf_gpio_latches_read_and_clear(0, GPIO_COUNT, input);
	}

	/* Process pin events. */
	if (status & NRF_GPIOTE_INT_IN_MASK)
	{
		mask = (uint32_t)NRF_GPIOTE_INT_IN0_MASK;

		for (i = 0; i < GPIOTE_CH_NUM; i++)
		{
			if (mask & status)
			{
				nrfx_gpiote_pin_t pin = nrf_gpiote_event_pin_get(i);
				nrf_gpiote_polarity_t polarity = nrf_gpiote_event_polarity_get(i);
				button_irq(pin, polarity);
			}
			mask <<= 1;
		}
	}
}

/**
 * @brief Function for configuring: PIN_IN pin for input, PIN_OUT pin for output,
 * and configures GPIOTE to give an interrupt on pin change.
 */
static void gpio_init(void)
{
	nrf_drv_gpiote_init();

	nrf_drv_gpiote_in_config_t in_config = GPIOTE_CONFIG_IN_SENSE_TOGGLE(true);
	in_config.pull = NRF_GPIO_PIN_PULLUP;

	nrfx_gpiote_evt_handler_t evt_handler = (void *)1;

	nrf_drv_gpiote_in_init(BUTTON_1, &in_config, evt_handler);
	nrf_drv_gpiote_in_init(BUTTON_2, &in_config, evt_handler);

	nrf_drv_gpiote_in_event_enable(BUTTON_1, true);
	nrf_drv_gpiote_in_event_enable(BUTTON_2, true);
}

/**
 * @brief Function for main application entry.
*============================================================================*/

int main(void)
{
	uart_init(0);

	bsp_board_init(BSP_INIT_LEDS);
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock / 1000);

	gpio_init();
	nanoOS_add_IRQ(GPIOTE_IRQn, in_pin_handler);

	nanoOS_semaphore_init(&button_pressed);
	nanoOS_task_init(&fsm, &fsm_task, 0);

    uart_init(0);

	nanoOS_init();

    while (true)
    {
        __NOP();
    }
}

/** @} doxygen end group definition */

/*==================[end of file]============================================*/
