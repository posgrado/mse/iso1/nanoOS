#include "uart.h"

#include "bsp.h"
#include "app_fifo.h"
#include "nrf_drv_uart.h"
#if defined(UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined(UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

#define UART_TX_BUF_SIZE 256 /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 256 /**< UART RX buffer size. */
#define UART_HWFC APP_UART_FLOW_CONTROL_DISABLED

static app_fifo_t m_rx_fifo; /**< RX FIFO buffer for storing data received on the UART until the application fetches them using app_uart_get(). */
static app_fifo_t m_tx_fifo; /**< TX FIFO buffer for storing data to be transmitted on the UART when TXD is ready. Data is put to the buffer on using app_uart_put(). */

char* itoa(int value, char* result, int base);

void uart_init(uint32_t err_code)
{
    const app_uart_comm_params_t comm_params =
        {
            RX_PIN_NUMBER,
            TX_PIN_NUMBER,
            RTS_PIN_NUMBER,
            CTS_PIN_NUMBER,
            UART_HWFC,
            false,
            NRF_UART_BAUDRATE_230400};

    app_uart_buffers_t buffers;
    static uint8_t rx_buf[UART_RX_BUF_SIZE];
    static uint8_t tx_buf[UART_TX_BUF_SIZE];

    buffers.rx_buf = rx_buf;
    buffers.rx_buf_size = sizeof(rx_buf);
    buffers.tx_buf = tx_buf;
    buffers.tx_buf_size = sizeof(tx_buf);

    // Configure buffer RX buffer.
    err_code = app_fifo_init(&m_rx_fifo, buffers.rx_buf, buffers.rx_buf_size);

    // Configure buffer TX buffer.
    err_code = app_fifo_init(&m_tx_fifo, buffers.tx_buf, buffers.tx_buf_size);

    nrf_drv_uart_config_t config = NRF_DRV_UART_DEFAULT_CONFIG;
    config.baudrate = (nrf_uart_baudrate_t)comm_params.baud_rate;
    config.hwfc = (comm_params.flow_control == APP_UART_FLOW_CONTROL_DISABLED) ? NRF_UART_HWFC_DISABLED : NRF_UART_HWFC_ENABLED;
    config.interrupt_priority = APP_IRQ_PRIORITY_LOWEST;
    config.parity = comm_params.use_parity ? NRF_UART_PARITY_INCLUDED : NRF_UART_PARITY_EXCLUDED;
    config.pselcts = comm_params.cts_pin_no;
    config.pselrts = comm_params.rts_pin_no;
    config.pselrxd = comm_params.rx_pin_no;
    config.pseltxd = comm_params.tx_pin_no;

    nrf_drv_uart_t app_uart_inst = NRF_DRV_UART_INSTANCE(APP_UART_DRIVER_INSTANCE);
    err_code = nrf_drv_uart_init(&app_uart_inst, &config, NULL);
}

void uart_error_handle(app_uart_evt_t *p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}

void print_msg(uint8_t nro_led, uint32_t t1, uint32_t t2)
{
	static nrf_drv_uart_t app_uart_inst = NRF_DRV_UART_INSTANCE(APP_UART_DRIVER_INSTANCE);
    uint8_t const str_led[] = "Led ";
    uint8_t const str_encendido[] = " encendido:\n\r";
    uint8_t const str_tiempo_encendido[] = "\t Tiempo encendido: ";
    uint8_t const str_ms[] = " ms\n\r";
    uint8_t const str_tiempo_entre_flancos_desc[] = "\t Tiempo entre flancos descendentes: ";
    uint8_t const str_tiempo_entre_flancos_asc[] = "\t Tiempo entre flancos ascendentes: ";
    char led[1];
    char time[6];
    nrf_drv_uart_tx(&app_uart_inst, str_led, sizeof(str_led));
    itoa(nro_led, led, 10);
    nrf_drv_uart_tx(&app_uart_inst, (const uint8_t *)led, sizeof(led));
    nrf_drv_uart_tx(&app_uart_inst, str_encendido, sizeof(str_encendido));
    nrf_drv_uart_tx(&app_uart_inst, str_tiempo_encendido, sizeof(str_tiempo_encendido));
    itoa(t1 + t2, time, 10);
    nrf_drv_uart_tx(&app_uart_inst, (const uint8_t *)time, sizeof(time));
    nrf_drv_uart_tx(&app_uart_inst, str_ms, sizeof(str_ms));
    nrf_drv_uart_tx(&app_uart_inst, str_tiempo_entre_flancos_desc, sizeof(str_tiempo_entre_flancos_desc));
    itoa(t1, time, 10);
    nrf_drv_uart_tx(&app_uart_inst, (const uint8_t *)time, sizeof(time));
    nrf_drv_uart_tx(&app_uart_inst, str_ms, sizeof(str_ms));
    nrf_drv_uart_tx(&app_uart_inst, str_tiempo_entre_flancos_asc, sizeof(str_tiempo_entre_flancos_asc));
    itoa(t2, time, 10);
    nrf_drv_uart_tx(&app_uart_inst, (const uint8_t *)time, sizeof(time));
    nrf_drv_uart_tx(&app_uart_inst, str_ms, sizeof(str_ms));
}

/**
 * C++ version 0.4 char* style "itoa":
 * Written by Lukás Chmela
 * Released under GPLv3.
 */
char* itoa(int value, char* result, int base) {
   // check that the base if valid
   if (base < 2 || base > 36) { *result = '\0'; return result; }

   char* ptr = result, *ptr1 = result, tmp_char;
   int tmp_value;

   do {
      tmp_value = value;
      value /= base;
      *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
   } while ( value );

   // Apply negative sign
   if (tmp_value < 0) *ptr++ = '-';
   *ptr-- = '\0';
   while(ptr1 < ptr) {
      tmp_char = *ptr;
      *ptr--= *ptr1;
      *ptr1++ = tmp_char;
   }
   return result;
}